#ifndef OOAM_LLSTK_H
#define OOAM_LLSTK_H
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

enum OOAM_LLSTK_FLAGS { OOAM_LLSTK_FREE_ON_POP = 1 << 0 };

#define OOAM_LLSTK_DECL(NAME_SUFFIX, TYPE)                                 \
    typedef struct llstk##NAME_SUFFIX##_node {                             \
        struct llstk##NAME_SUFFIX##_node* nxt;                             \
        TYPE val;                                                          \
    } llstk##NAME_SUFFIX##_node;                                           \
                                                                           \
    typedef struct llstk##NAME_SUFFIX {                                    \
        llstk##NAME_SUFFIX##_node* top;                                    \
        size_t size;                                                       \
    } llstk##NAME_SUFFIX;                                                  \
                                                                           \
    inline llstk##NAME_SUFFIX llstk##NAME_SUFFIX##_new(void);              \
                                                                           \
    TYPE* llstk##NAME_SUFFIX##_add(llstk##NAME_SUFFIX* llstk);             \
                                                                           \
    TYPE* llstk##NAME_SUFFIX##_cadd(llstk##NAME_SUFFIX* llstk, TYPE val);  \
                                                                           \
    TYPE* llstk##NAME_SUFFIX##_padd(llstk##NAME_SUFFIX* llstk, TYPE* val); \
                                                                           \
    void llstk##NAME_SUFFIX##_pop(llstk##NAME_SUFFIX* llstk);              \
                                                                           \
    void llstk##NAME_SUFFIX##_free(llstk##NAME_SUFFIX* llstk);

#define OOAM_LLSTK_IMPL(NAME_SUFFIX, TYPE, FREE_FN, FLAGS)                    \
    inline llstk##NAME_SUFFIX llstk##NAME_SUFFIX##_new(void) {                \
        return (llstk##NAME_SUFFIX){.top = NULL, .size = 0};                  \
    }                                                                         \
                                                                              \
    TYPE* llstk##NAME_SUFFIX##_add(llstk##NAME_SUFFIX* llstk) {               \
        llstk##NAME_SUFFIX##_node* node = (llstk##NAME_SUFFIX##_node*)malloc( \
            sizeof(llstk##NAME_SUFFIX##_node));                               \
        if (node == NULL) {                                                   \
            perror("llstk" #NAME_SUFFIX                                       \
                   ":"                                                        \
                   "error allocating new node");                              \
            exit(EXIT_FAILURE);                                               \
        }                                                                     \
                                                                              \
        /* assumes this is NULL if llstk is empty */                          \
        node->nxt = llstk->top;                                               \
        llstk->top = node;                                                    \
        ++llstk->size;                                                        \
                                                                              \
        return &node->val;                                                    \
    }                                                                         \
                                                                              \
    TYPE* llstk##NAME_SUFFIX##_cadd(llstk##NAME_SUFFIX* llstk, TYPE val) {    \
        llstk##NAME_SUFFIX##_node* node = (llstk##NAME_SUFFIX##_node*)malloc( \
            sizeof(llstk##NAME_SUFFIX##_node));                               \
        if (node == NULL) {                                                   \
            perror("llstk" #NAME_SUFFIX                                       \
                   ":"                                                        \
                   "error allocating new node");                              \
            exit(EXIT_FAILURE);                                               \
        }                                                                     \
                                                                              \
        node->val = val;                                                      \
                                                                              \
        /* assumes this is NULL if llstk is empty */                          \
        node->nxt = llstk->top;                                               \
        llstk->top = node;                                                    \
        ++llstk->size;                                                        \
                                                                              \
        return &node->val;                                                    \
    }                                                                         \
                                                                              \
    TYPE* llstk##NAME_SUFFIX##_padd(llstk##NAME_SUFFIX* llstk, TYPE* val) {   \
        llstk##NAME_SUFFIX##_node* node = (llstk##NAME_SUFFIX##_node*)malloc( \
            sizeof(llstk##NAME_SUFFIX##_node));                               \
        if (node == NULL) {                                                   \
            perror("llstk" #NAME_SUFFIX                                       \
                   ":"                                                        \
                   "error allocating new node");                              \
            exit(EXIT_FAILURE);                                               \
        }                                                                     \
                                                                              \
        node->val = *val;                                                     \
                                                                              \
        /* assumes this is NULL if llstk is empty */                          \
        node->nxt = llstk->top;                                               \
        llstk->top = node;                                                    \
        ++llstk->size;                                                        \
                                                                              \
        return &node->val;                                                    \
    }                                                                         \
                                                                              \
    void llstk##NAME_SUFFIX##_pop(llstk##NAME_SUFFIX* llstk) {                \
        llstk##NAME_SUFFIX##_node* node = llstk->top;                         \
        llstk->top = node->nxt;                                               \
        --llstk->size;                                                        \
        if (FLAGS & OOAM_LLSTK_FREE_ON_POP) {                                 \
            void (*func)(TYPE*) = FREE_FN;                                    \
            func(&(node->val));                                               \
        }                                                                     \
        free(node);                                                           \
    }                                                                         \
                                                                              \
    void llstk##NAME_SUFFIX##_free(llstk##NAME_SUFFIX* llstk) {               \
        llstk##NAME_SUFFIX##_node* node;                                      \
        while ((node = llstk->top)) {                                         \
            if (FREE_FN != NULL) {                                            \
                void (*func)(TYPE*) = FREE_FN;                                \
                func(&(node->val));                                           \
            }                                                                 \
                                                                              \
            llstk->top = node->nxt;                                           \
            free(node);                                                       \
        }                                                                     \
    }

#endif
