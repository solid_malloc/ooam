#ifndef OOAM_LLSTK_TEST_H
#define OOAM_LLSTK_TEST_H

#include "ooam_llstk.h"

#include "test_ls_1.h"

OOAM_LLSTK_DECL(_test_ls_1, struct test_ls_1)
OOAM_LLSTK_IMPL(_test_ls_1, struct test_ls_1, NULL, 0)

OOAM_LLSTK_DECL(_test_int, int)
OOAM_LLSTK_IMPL(_test_int, int, NULL, 0)

#endif
