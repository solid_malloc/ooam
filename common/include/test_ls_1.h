#ifndef TEST_LS_1_H
#define TEST_LS_1_H
#include <stdint.h>

// a large struct (ls) in terms of memory it takes up, as well as some utils to
// more easily use it in tests 
struct test_ls_1 {
    uint8_t unint8_arr[4096];
    char* charptr_arr[1024];
    int number;
};

struct test_ls_1 test_ls_1_new_zero(void);

#endif
