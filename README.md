# OO(ps! )A(ll )M(acros!)
![logo](./logo.png)

## description
OOAM is a collection of data structures defined as macros, such that they can be adapted to different data types. the goal is to emulate generics in C while taking performance, type safety, and ease-of-use into account.