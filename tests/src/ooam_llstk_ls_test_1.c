#include <stdio.h>

#include "llstk_test_ls_1.h"
#include "test_ls_1.h"

static int new_add_free_1(void) {
    llstk_test_ls_1 stk_ls = llstk_test_ls_1_new();

    struct test_ls_1 ls1 = test_ls_1_new_zero();
    ls1.number = 0;

    struct test_ls_1 ls2 = test_ls_1_new_zero();
    ls2.number = 1;

    struct test_ls_1 ls3 = test_ls_1_new_zero();
    ls3.number = 2;

    *llstk_test_ls_1_add(&stk_ls) = ls1;
    *llstk_test_ls_1_add(&stk_ls) = ls2;
    *llstk_test_ls_1_add(&stk_ls) = ls3;

    llstk_test_ls_1_node* node = stk_ls.top;
    int ind = 2;

    while (node) {
        if (node->val.number != ind) {
            fprintf(stderr, "unexpected number value %d in node %d\n",
                    node->val.number, 2 - ind);
            return 1;
        }
        node = node->nxt;
        --ind;
    }

    if (ind != -1) {
        fprintf(stderr,
                "unexpected exit from loop"
                "at index %d\n",
                ind);
        return 1;
    }

    if (stk_ls.size != 3) {
        fprintf(stderr, "unexpected stack size %zu\n", stk_ls.size);
        return 1;
    }

    llstk_test_ls_1_free(&stk_ls);
    return 0;
}

int new_add_pop_free_1(void) {
    llstk_test_ls_1 stk_ls = llstk_test_ls_1_new();

    struct test_ls_1 ls1 = test_ls_1_new_zero();
    ls1.number = 0;

    struct test_ls_1 ls2 = test_ls_1_new_zero();
    ls2.number = 1;

    struct test_ls_1 ls3 = test_ls_1_new_zero();
    ls3.number = 2;

    *llstk_test_ls_1_add(&stk_ls) = ls1;
    *llstk_test_ls_1_add(&stk_ls) = ls2;
    *llstk_test_ls_1_add(&stk_ls) = ls3;

    int iteration_count = 0;
    while (stk_ls.size > 0) {
        size_t start_size = stk_ls.size;

        llstk_test_ls_1_pop(&stk_ls);
        if (stk_ls.size != start_size - 1) {
            fprintf(stderr, "unexpected stack size %zu", stk_ls.size);
            return 1;
        }

        ++iteration_count;
    }

    if (iteration_count != 3) {
        fprintf(stderr, "unexpected iteration count %d\n", iteration_count);
        return 1;
    }

    if (stk_ls.top != NULL) {
        fprintf(stderr, "empty stack top is not NULL\n");
        return 1;
    }

    return 0;
}

int main(void) {
    int retval = 0;

    if ((retval |= new_add_free_1())) {
        perror("new_add_free_1 failed");
    }

    if ((retval |= new_add_pop_free_1())) {
        perror("new_add_pop_free_1 failed");
    }

    return retval;
}
